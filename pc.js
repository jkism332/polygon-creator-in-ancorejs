Game.settings.set({
  title: "Polygon Creator",
  target: "chrome_desktop"
})

Game.onload = () => {
  Game.apply();
  r_main.load();
}


r_main = new Room ({
  creator: [[]]
}, {
  main: new Background("white", "solid")
}, () => {
  console.log("started polygon creator");
  view.x = -screen.width / 2;
  view.y = -screen.height / 2;
},600,600,60);

poly = [];

function create_points() {
  obj.point = [];
  for (let i = 0; i < poly.length; i++) {
    obj.point.push(new point$(i));
  }
}

creator$ = class extends Thing {
  constructor() {
    super();
    this.x = 0;
    this.y = 0;
    this.pnt = 0;
    this.clicked = false;
    this.loaded = false;
    this.center = false;
  }
  step() {
    this.center = when(this.center, Key.check("space"), () => {
      poly = Polygon.center(poly);
      create_points();
    })
  }
  draw() { 
    if ((image != null)) {
      ctx.drawImage(image, rx, ry, wid, hig);
      ctx.globalAlpha = 0.75;
    }
    
    if (poly.length > 0) {
      Polygon.draw(poly);
    }
    ctx.globalAlpha = 1;
  }

  on_mouse() {
    for (let i in obj.point) {
      if ((Mouse.X >= obj.point[i].x - obj.point[i].height / 2)&&
      (Mouse.Y >= obj.point[i].y - obj.point[i].height / 2)&&
      (Mouse.X <= obj.point[i].x + obj.point[i].height / 2)&&
      (Mouse.Y <= obj.point[i].y + obj.point[i].height / 2))  {
        this.clicked = true;
      }
    }
    if (!(this.clicked)) {
      console.log(Mouse.X, Mouse.Y);
      poly.push({x: Mouse.X, y: Mouse.Y});
      obj.point.push(new point$(this.pnt));
      console.log("updated polygon");
      console.log(poly);
      this.pnt++;
    }
  }
}

point$ = class extends Thing {
  constructor(pnt) {
    super();
    this.x = poly[pnt].x;
    this.y = poly[pnt].y;
    this.width = this.height = 5;
    this.pnt = pnt;
  }
  step() {
    this.x = poly[this.pnt].x;
    this.y = poly[this.pnt].y;
    if (!(Key.check("mouse"))) {
      this.clicked = false;
    }

    if (this.clicked) {
      poly[this.pnt] = {x: Mouse.X, y: Mouse.Y};
      obj.creator[0].clicked = false;
    }

    

   
  }
  draw() {
    ctx.fillStyle = "lime";
    ctx.fillRect(this.x - this.width / 2, this.y - this.width / 2, this.width, this.height);
    if (((Mouse.X >= this.x - this.height / 2)&&
    (Mouse.Y >= this.y - this.height / 2)&&
    (Mouse.X <= this.x + this.height / 2)&&
    (Mouse.Y <= this.y + this.height / 2))||(this.clicked)) {
      ctx.fillStyle = "black";
      try {
        ctx.font = "30px Arial";
        ctx.fillText([poly[this.pnt].x,poly[this.pnt].y], this.x, this.y - 10);
      } catch(e) {}
    }
  }
  on_mouse() {
    
    if (
    (Mouse.X >= this.x - this.height / 2)&&
    (Mouse.Y >= this.y - this.height / 2)&&
    (Mouse.X <= this.x + this.height / 2)&&
    (Mouse.Y <= this.y + this.height / 2)) {
      if (this.del) {
        poly.splice(this.pnt, 1);
        obj.creator[0].pnt --; 
        obj.creator[0].clicked = false;
        for (let i in obj.point) {
          if (obj.point[i].pnt > this.pnt) {
            obj.point[i].pnt --;
          }
        }
        this.destroy();
      }
      this.clicked = true;
      this.del = true;
      setTimeout(() => {
        this.del = false;
      }, 1000 / 5);
      return true;
    } 
  }
}

def("creator", "point");

p = prompt("Input image url or dont");
if (p) {
  image = new Image;
  image.src = p;
  image.onload = () => {
    wid = (image.width > image.height ? 400 : 400 / image.height * image.width );
    hig = (image.height > image.width ? 400 : 400 / image.width * image.height );
    rx  = (0 - wid) / 2;
    ry  = (0 - hig) / 2;
  }
} else {
  image = null;
}