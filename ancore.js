////////////////////////////
/* This is a game engine 
   based on Game Maker    */
////////////////////////////

////////////////////////////

////////////////////////////
/* Initializes The Screen */
////////////////////////////

const screen = document.createElement("canvas");
screen.width = screen.height = 600;
const ctx = screen.getContext("2d");
var game_loop;

window.onload = () => {
  document.body.appendChild(screen);
  screen.style = "padding: 0; margin: 0";
  ctx.imageSmoothingEnabled = false;
  ctx.textAlign = "center";
  window.requestAnimationFrame(draw_all);
  document.addEventListener("keydown", Key.add);
  document.addEventListener("keyup", Key.remove);
  document.addEventListener("mouseup", Key.mouse_up);
  screen.addEventListener("mousemove", Key.log_mouse);
  screen.addEventListener("mousedown", Key.mouse_down);
  screen.addEventListener("mousewheel", Key.mouse_whl);
  if (Load.max < 1) {
    Game.onload();
  }
};

window.onbeforeunload = () => {
  Game.onend();
}

/////////////////////////////
/*     Game Engine Part    */
/////////////////////////////

const obj = {};
const img = {};
const snd = {};
const global = {};
var bck = {};
var room;
var draw_queue = [];
var mouse_hierarchy = [];
var step_count = 0;
var paused = false;
var loaded = false;

const IMG = "image";
const SND = "audio";
const VID = "video";
const GIF = "gif";

const step_all = () => {
  step_count++;
  Key.update_mouse();
  draw_queue = [];
  mouse_hierarchy = [];
  for (let i in obj) {
    for (let e in obj[i]) {
      try {
        obj[i][e].update();
        for (let t in obj[i][e].loops) {
          obj[i][e].loops[t].step();
        }
      } catch (e) {}
    }
  }
  view.update();
};

const draw_all = () => {
  ctx.clearRect(view.x, view.y, screen.width, screen.height);
  ctx.save();
  ctx.translate(-view.x, -view.y);
  for (let i in bck) {
    bck[i].draw();
  }
  for (let e in draw_queue) {
    for (let t in draw_queue[e]) {
      draw_queue[e][t]();
    }
  }
  ctx.restore();
  if (!paused) {
    requestAnimationFrame(draw_all);
  }
  if (Load.opacity > 0) {
    ctx.globalAlpha = Load.opacity;
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, screen.width, screen.height);
    ctx.globalAlpha = 1;
  }
};

const stop = () => {
  clearInterval(game_loop);
  game_loop = null;
  paused = true;
};

const resume = fps => {
  paused = false;
  if (game_loop == null) {
    requestAnimationFrame(draw_all);
    game_loop = setInterval(step_all, 1000 / fps);
  }
};

const isUndefined = x => {
  return x == null;
};

//////////////////////
/*     Functions    */
//////////////////////

/** Brings a value closer to the other one by the amount */
function lerp(val, val1, amt) {
  return val + (val1 - val) * amt;
}

function approach(val, val1, amt) {
  if (val > val1) {
    return clamp(val - amt, val1, val);
  } else if (val < val1) {
    return clamp(val + amt, val, val1);
  } else {
    return val;
  }
}

/** Clamps a value to min , max */
function clamp(val, min, max) {
  if (val < min) {
    return min;
  } else if (val > max) {
    return max;
  } else {
    return val;
  }
}
/** Wraps value */
function wrap(val, min, max) {
  if (val < min) {
    return max;
  } else if (val > max) {
    return min;
  } else {
    return val;
  }
}
/** Wraps value precisely  */
function wrap_precise(val, min, max) {
  if (val < min) {
    return max - (min - val);
  } else if (val > max) {
    return min + (val - max);
  } else {
    return val;
  }
}

function wave(from, to, duration, offset) {
  let pho = (to - from) / 2;
  return (
    from +
    pho +
    Math.sin(
      (new Date() * 0.001 + duration * offset) / duration * (Math.PI * 2)
    ) *
      pho
  );
}

function flip(val, min, max) {
  return min + max - val;
}

function chance(perc) {
  return Math.random() <= perc / 100;
}

/** Detects collisions between objects */
function place_meeting(that, other, x = that.x, y = that.y, angle = that.angle, poly = that.poly) {
  let px = x;
  let py = y;
  let ang = angle;
  let pol = poly;
  let is = false;
  let obj = [];
  let p0 = Polygon.rotate(Polygon.translate(pol, px, py), Angle.rad(ang));
  let p1;
  for (let i in other) {
    p1 = Polygon.rotate(
      Polygon.translate(other[i].mask, other[i].x, other[i].y),
      Angle.rad(other[i].angle)
    );
    if (Polygon.collides(p0, p1)) {
      is = true;
      obj.push(i);
    }
  }
  return { is: is, other: obj };
}

function outside_room(that) {
  let p = Polygon.rotate(Polygon.transfer(that.mask, that.x, that.y));
  let c = Polygon.get_corner(p);
  return (
    room.width < c.maxX || 0 > c.minX || 0 > c.minY || room.height < c.maxY
  );
}

function preload(type, name, src) {
  if (type == "image") {
    Load.start();
    img[name] = new Image();
    img[name].src = src;
    img[name].onload = () => {
        Load.stop();
    };
  } else if (type == "audio") {
    Load.start();
    snd[name] = new Audio();
    snd[name].src = src;
    snd[name].oncanplaythrough = () => {
      Load.stop();
      snd[name].oncanplaythrough = () => {};
    };
  }
}

function def(...names) {
  for (let i in names) {
    obj[names[i]] = [];
  }
  
}

function when(bool, cond, fn) {
  if (!bool && !cond) {
    return false;
  } else if (!bool && cond) {
    fn();
    return true;
  } else if (bool && !cond) {
    return false;
  } else if (bool && cond) {
    return true;
  }
}

function save(obj, dur) {
  let keys = Object.keys(obj);
  let exp = new Date();
  exp.setTime(exp.getTime() + dur*24*60*60*1000);
  for (let i in keys) {
    let obb = obj[keys[i]];
    document.cookie = keys[i] + "=" + obb + ";" + "expires=" + exp + ";";
  }
}

function load(name) {
  let temp = window[name];
  let cookie = decodeURIComponent(document.cookie).split(";");
  for (let i in cookie) {
    let pho = cookie[i].split("=");
    if (i != 0) {
      pho[0] = pho[0].slice(1);
    }
    if (pho[0] != "_ga") {
      temp[pho[0]] = eval(pho[1]);
    }
  }
  window[name] = temp;
  setTimeout(() => {
    Savefile.onload();
  }, 100);
  return Savefile;

}
//////////////////////////
/* Functions in objects */
//////////////////////////
const Random = {
  int(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  },
  float(min, max) {
    return Math.random() * (max - min) + min;
  },
  choose(args) {
    return args[Math.floor(Math.random() * args.length)];
  },
  rgb() {
    return "rgb(" + Random.int(0,256) + "," + Random.int(0,256) + "," + Random.int(0,256) + ")";
  },
  rgba() {
    return "rgba(" + Random.int(0,256) + "," + Random.int(0,256) + "," + Random.int(0,256) + "," + Math.random() + ")";
  },
}

const Time = {
  ms() {
    return new Date().valueOf();
  },
  toSec(time) {
    return Math.floor(time / 60);
  }
}

const Savefile = {
  onload() {
    console.log("dady");
  }
}

const Load = {
  opacity: 0,
  queue: 0,
  max: 0,
  start() {
    stop();
    if (Load.queue == 0) {
      requestAnimationFrame(Load.draw);
    }
    Load.queue++;
    Load.max++;
  },
  stop() {
    Load.queue--;
    if (Load.queue < 1) {
      Load.opacity = 1;
      Load.fade = setInterval(() => {
        Load.opacity -= 0.05;
        if (Load.opacity == 0) {
          clearInterval(Load.fade);
        }
      }, 1000 / 30);
      if (room) {
        resume(room.fps);
      } else {
        Game.onload();
      }
    }
  },

  angle: 0,
  dist: 0,
  done: 0,
  spd: 0,
  dur: 1,
  sz: 5,
  ix: 0,
  text: ["A N C O R E", ". J S"],
  bar: 0,
  sd: [],

  draw() {
    Load.dist = wave(20, 100, Load.dur, 1);
    Load.spd = wave(10, 0, Load.dur, 1);
    Load.angle += Load.spd;
    Load.angle = wrap_precise(Load.angle, 0, 360);
    Load.sd = [
      [-Load.dist, -Load.dist],
      [Load.dist, -Load.dist],
      [Load.dist, Load.dist],
      [-Load.dist, Load.dist]
    ];
    Load.bar = lerp(Load.bar, (Load.max - Load.queue) / Load.max * 200, 0.1);
    Load.bar = Math.ceil(Load.bar);
    Load.bar = Load.bar ? Load.bar : 0;
    Load.bar = clamp(Load.bar, 0, 200);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, screen.width, screen.height);
    ctx.save();
    ctx.translate(screen.width / 2, screen.height / 2);
    ctx.rotate(Angle.rad(Load.angle));
    ctx.strokeStyle = "white";
    ctx.beginPath();
    ctx.strokeRect(-Load.dist, -Load.dist, Load.dist * 2, Load.dist * 2);
    ctx.stroke();
    for (let i in Load.sd) {
      ctx.beginPath();
      ctx.arc(Load.sd[i][0], Load.sd[i][1], Load.sz, 0, Math.PI * 2);
      ctx.fill();
      ctx.stroke();
    }
    ctx.rotate(-Angle.rad(Load.angle));
    if (Load.bar > 0) {
      ctx.fillStyle = "white";
      ctx.fillRect(-100, 160, Load.bar, 1);
    }
    ctx.textAlign = "center";
    ctx.font = "10px Encode Sans Expanded";
    ctx.strokeText(Load.bar / 2 + " %", 0, 180);
    ctx.globalAlpha = wave(0, 1, Load.dur, 1);
    done = when(Load.done, ctx.globalAlpha < 0.1, () => {
      Load.ix = flip(Load.ix, 0, 1);
    });
    ctx.font = "20px Encode Sans Expanded";
    ctx.strokeText(Load.text[Load.ix], 0, 0);
    ctx.restore();
    if (Load.queue >= 1) {
      requestAnimationFrame(Load.draw);
    }
  }
};

/** @description Keeps track of mouse coords */
const Mouse = {
  clientX: 0,
  clientY: 0,
  X: 0,
  Y: 0,
  scroll: 0
};

/** @description Includes everything to do with input */
const Key = {
  /**
   * @description Holds the keyCodes of keys that are currently pressed down
   */
  holder: [],
  /**
   * @description Checks for a key being held down
   * @param {string} kc The key to check for ("W" or "space", "enter" ...)
   */
  check(kc) {
    switch (kc) {
      case "left":
        kc = 37;
        break;
      case "up":
        kc = 38;
        break;
      case "right":
        kc = 39;
        break;
      case "down":
        kc = 40;
        break;
      case "space":
        kc = 32;
        break;
      case "ctrl":
        kc = 17;
        break;
      case "alt":
        kc = 18;
        break;
      case "shift":
        kc = 16;
        break;
      case "enter":
        kc = 13;
        break;
      case "Q":
        kc = 81;
        break;
      case "W":
        kc = 87;
        break;
      case "E":
        kc = 69;
        break;
      case "R":
        kc = 82;
        break;
      case "T":
        kc = 84;
        break;
      case "Y":
        kc = 89;
        break;
      case "U":
        kc = 85;
        break;
      case "I":
        kc = 73;
        break;
      case "O":
        kc = 79;
        break;
      case "P":
        kc = 80;
        break;
      case "A":
        kc = 65;
        break;
      case "S":
        kc = 83;
        break;
      case "D":
        kc = 68;
        break;
      case "F":
        kc = 70;
        break;
      case "G":
        kc = 71;
        break;
      case "H":
        kc = 72;
        break;
      case "J":
        kc = 74;
        break;
      case "K":
        kc = 75;
        break;
      case "L":
        kc = 76;
        break;
      case "Z":
        kc = 90;
        break;
      case "X":
        kc = 88;
        break;
      case "C":
        kc = 67;
        break;
      case "V":
        kc = 86;
        break;
      case "B":
        kc = 66;
        break;
      case "N":
        kc = 78;
        break;
      case "M":
        kc = 77;
        break;
    }
    if (Key.holder.includes(kc)) {
      return true;
    } else {
      return false;
    }
  },
  /**
   * Native functions, you shall not use them
   */
  add(evt) {
    if (!Key.holder.includes(evt.keyCode)) {
      Key.holder.push(evt.keyCode);
    }
  },
  remove(evt) {
    Key.holder.splice(Key.holder.indexOf(evt.keyCode), 1);
  },
  log_mouse(evt) {
    Mouse.clientX = evt.clientX;
    Mouse.clientY = evt.clientY;
  },
  update_mouse(evt) {
    let cr = screen.getBoundingClientRect();
    Mouse.X = Mouse.clientX + view.x - cr.x;
    Mouse.Y = Mouse.clientY + view.y - cr.y;
    Mouse.scroll = 0;
  },
  mouse_down() {
    Key.add({ keyCode: "mouse" });
    let pho = false;
    outer: for (let e in mouse_hierarchy) {
      for (let t in mouse_hierarchy[e]) {
        if(mouse_hierarchy[e][t]()) {
          break; 
          break outer;
        }
      }
    }
  },
  mouse_up() {
    Key.remove({ keyCode: "mouse" });
  },
  mouse_whl(evt) {
    let sc = evt.deltaY / 100;
    for (let i in obj) {
      for (let e in obj[i]) {
        obj[i][e].on_scroll(sc);
      }
    }
  }
  /** End of Native functions */
};

/** Used to edit coordinates */
const Point = {
  /** Rotates a point around origin */
  rotate(po, oo, rad) {
    let p = po;
    let o = oo;
    let s = Math.sin(rad);
    let c = Math.cos(rad);

    // translate point back to origin:
    p.x -= o.x;
    p.y -= o.y;

    // rotate point
    let xnew = p.x * c - p.y * s;
    let ynew = p.x * s + p.y * c;

    p.x = xnew + o.x;
    p.y = ynew + o.y;
    return p;
  }
};

const Angle = {
  /** Returns an angle between 2 points */
  between(p1, p2) {
    return Angle.deg(Math.atan2(p1.y - p2.y, p1.x - p2.x));
  },
  /** Converts degrees to radians */
  rad(deg) {
    return (deg - 180) * Math.PI / 180;
  },
  /** Converts radians to degrees */
  deg(rad) {
    return rad * 180 / Math.PI + 180;
  },
  /** Interpolates between two angles which are in degrees (from 0 to 360) */
  relp(ang0, ang1, amt) {
    let ang = [Math.max(ang0, ang1), Math.min(ang0, ang1)];
    let alpha = 360 - ang[0] + ang[1];
    let beta = ang[0] - ang[1];
    if (alpha < beta) {
      if (ang0 < 180) {
        return wrap_precise(lerp(ang0, ang0 - alpha, amt), 0, 360);
      } else {
        return wrap_precise(lerp(ang0, ang0 + alpha, amt), 0, 360);
      }
    } else {
      return lerp(ang0, ang1, amt);
    }
  },
  dir_x(ang) {
    return Math.cos(Angle.rad(ang)) * -1;
  },
  dir_y(ang) {
    return Math.sin(Angle.rad(ang)) * -1;
  }
};

const Polygon = {
  new_rect(w, h) {
    return [{ x: 0, y: 0 }, { x: w, y: 0 }, { x: w, y: h }, { x: 0, y: h }];
  },

  new_circle: r => {
    let pho = Math.floor(r * 0.36);
    pho = clamp(pho, -180, 180);
    let new_polygon = [];
    for (let i = 0; i < pho - 1; i++) {
      new_polygon.push(
        Point.rotate({ x: r, y: 0 }, { x: 0, y: 0 }, Angle.rad(i * 360 / pho))
      );
    }
    return new_polygon;
  },

  rotate: (polygon, rad, origin) => {
    if (origin == null) {
      var origin = Polygon.get_center(polygon);
    }
    let new_polygon = [];
    for (i in polygon) {
      new_polygon.push(Point.rotate(polygon[i], origin, rad));
    }
    return new_polygon;
  },

  draw: (polygon, color) => {
    ctx.beginPath();
    ctx.fillStyle = color == null ? "violet" : color;
    ctx.strokeStyle = "blue";
    ctx.lineWidth = 2;
    ctx.moveTo(polygon[0].x, polygon[0].y);
    for (let i in polygon) {
      ctx.lineTo(polygon[i].x, polygon[i].y);
    }
    ctx.lineTo(polygon[0].x, polygon[0].y);
    ctx.fill();
    ctx.stroke();
  },

  get_center(poly) {
    let px = [];
    let py = [];
    for (let i in poly) {
      px.push(poly[i].x);
      py.push(poly[i].y);
    }
    let minX = Math.min(...px);
    let maxX = Math.max(...px);
    let minY = Math.min(...py);
    let maxY = Math.max(...py);
    return {
      x: minX + (maxX - minX) / 2,
      y: minY + (maxY - minY) / 2
    };
  },

  get_corner(poly) {
    let px = [];
    let py = [];
    for (let i in poly) {
      px.push(poly[i].x);
      py.push(poly[i].y);
    }
    let minX = Math.min(...px);
    let maxX = Math.max(...px);
    let minY = Math.min(...py);
    let maxY = Math.max(...py);
    return { minX: minX, maxX: maxX, minY: minY, maxY: maxY };
  },

  center(poly) {
    let new_polygon = [];
    let center = Polygon.get_center(poly);
    for (let i in poly) {
      new_polygon.push({
        x: poly[i].x - center.x,
        y: poly[i].y - center.y
      });
    }
    return new_polygon;
  },

  offset(polygon, x, y) {
    let new_polygon = [];
    for (let i in polygon) {
      new_polygon.push({
        x: polygon[i].x + x,
        y: polygon[i].y + y
      });
    }
    return new_polygon;
  },

  translate(poly, x, y) {
    let new_polygon = [];
    let center = Polygon.center(poly);
    for (let i in poly) {
      new_polygon.push({
        x: center[i].x + x,
        y: center[i].y + y
      });
    }
    return new_polygon;
  },

  scale(poly, xmult, ymult) {
    let new_polygon = [];
    for (let i in poly) {
      new_polygon.push({ x: poly[i].x * xmult, y: poly[i].y * ymult });
    }
    return new_polygon;
  },

  size(poly, wid, hig) {
    let new_polygon = [];
    let co = Polygon.get_corner(poly);
    let xmult = wid == null ? 1 : wid / (co.maxX - co.minX);
    let ymult = hig == null ? 1 : hig / (co.maxY - co.minY);
    for (let i in poly) {
      new_polygon.push({ x: poly[i].x * xmult, y: poly[i].y * ymult });
    }
    return new_polygon;
  },

  make_object(poly) {
    let new_polygon = [];
    for (let i in poly) {
      new_polygon.push({
        x: poly[i][0],
        y: poly[i][1]
      });
    }
    return new_polygon;
  },

  collides(a, b) {
    var polygons = [a, b];
    var minA, maxA, projected, i, i1, j, minB, maxB;
    for (i = 0; i < polygons.length; i++) {
      // for each polygon, look at each edge of the polygon, and determine if it separates
      // the two shapes
      var polygon = polygons[i];
      for (i1 = 0; i1 < polygon.length; i1++) {
        // grab 2 vertices to create an edge
        var i2 = (i1 + 1) % polygon.length;
        var p1 = polygon[i1];
        var p2 = polygon[i2];
        // find the line perpendicular to this edge
        var normal = { x: p2.y - p1.y, y: p1.x - p2.x };
        minA = maxA = undefined;
        // for each vertex in the first shape, project it onto the line perpendicular to the edge
        // and keep track of the min and max of these values
        for (j = 0; j < a.length; j++) {
          projected = normal.x * a[j].x + normal.y * a[j].y;
          if (isUndefined(minA) || projected < minA) {
            minA = projected;
          }
          if (isUndefined(maxA) || projected > maxA) {
            maxA = projected;
          }
        }
        // for each vertex in the second shape, project it onto the line perpendicular to the edge
        // and keep track of the min and max of these values
        minB = maxB = undefined;
        for (j = 0; j < b.length; j++) {
          projected = normal.x * b[j].x + normal.y * b[j].y;
          if (isUndefined(minB) || projected < minB) {
            minB = projected;
          }
          if (isUndefined(maxB) || projected > maxB) {
            maxB = projected;
          }
        }
        // if there is no overlap between the projects, the edge we are looking at separates the two
        // polygons, and we know there is no overlap
        if (maxA < minB || maxB < minA) {
          return false;
        }
      }
    }
    return true;
  }
};

const Instance = {
  spawn(name, args = []) {
    let temp = eval(name + "$");
    obj[name].push(new temp(...args));
  },
  destroy(name, id = 0) {
    obj[name][id].destroy();
  }
};

const Game = {
  onload() {},
  onend() {},
  settings: {
    set: (stg) => {
      let keys = Object.keys(stg);
      for (let i in keys) {
        Game.settings[keys[i]] = stg[keys[i]];
      }
    },
    title: "AnCore Game",
    target: "chrome_desktop"
  },
  apply() {
    document.title = Game.settings.title;
  }

};

const Draw = {
  angle(angle, x, y, fn) {
    ctx.save();
    ctx.translate(x,y);
    ctx.rotate(Angle.rad(angle));
    fn();
    ctx.restore();
  },
  alpha(alpha, fn) {
    let orig = ctx.globalAlpha;
    ctx.globalAlpha = alpha;
    fn();
    ctx.globalAlpha = orig;
  },
  flip(x, y, h, v, fn) {
    ctx.save();
    ctx.translate(x,y);
    ctx.scale(h, v);
    fn();
    ctx.restore();
  },
  text(text, x, y, sz = 30, color = "white", font = Game.settings.font, align = "center", maxw = Infinity) {
    ctx.save();
    ctx.font = sz + "px " + font;
    ctx.textAlign = align;
    ctx.fillStyle = color;
    ctx.fillText(text, x, y, maxw);
    ctx.restore();
  } 
}

/////////////////
/*   Classes   */
/////////////////
const Vector = class {
  constructor(x = 0, y = 0, z = 0) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}


const View = class {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  follow(object_name, offx, offy) {
    this.obj = object_name;
    this.offx = screen.width / 2 - (offx == null ? 0 : offx);
    this.offy = screen.height / 2 - (offy == null ? 0 : offy);
    this.dofollow = () => {
      this.x = obj[this.obj][0].x - this.offx;
      this.y = obj[this.obj][0].y - this.offy;
    };
  }
  dofollow() {}
  clamp(x, y, x1, y1) {
    this.clampx = [x,x1];
    this.clampy = [y,y1];
    this.doclamp = () => {
      this.x = clamp(this.x, this.clampx[0], this.clampx[1] - screen.width);
      this.y = clamp(this.y, this.clampy[0], this.clampy[1] - screen.height);
    }
  }
  doclamp() {}
  update() {
    this.dofollow();
    this.doclamp();
  }
  move(x, y) {
    this.x += x;
    this.y += y;
    this.update();
  } 
};

class Room {
  constructor(ob, bk, onload, wid, hig, fps) {
    /* Stores info of a room which can be loaded */
    this.width = wid;
    this.height = hig;
    this.fps = fps;
    this.ob = (function() {
      if (typeof ob == "object") {
        return ob;
      } else {
        return {};
      }
    })();
    this.bk = (function() {
      if (typeof bk == "object") {
        return bk;
      } else {
        return {};
      }
    })();
    this.onload = (function() {
      if (typeof onload == "function") {
        return onload;
      } else {
        return function() {};
      }
    })();
  }
  load() {
    room = this;
    screen.width = this.width;
    screen.height = this.height;
    stop();
    resume(this.fps);
    for (let i in obj) {
      for (let e in obj[i]) {
        if (!(obj[i][e].persistant)) {
          obj[i][e].destroy();
        }
      }
    }
    let keys = Object.keys(this.ob);
    for (let i in this.ob) {
      for (let e in this.ob[i]) {
        Instance.spawn(i, this.ob[i][e]);
      }
    }
    bck = this.bk;
    this.onload();
  }
}
const Background = class {
  constructor(name, type) {
    //source = path to the image // or if type is solid source = color code
    this.types = ["tiled", "fullscreen", "single", "solid"];
    if (!(type == "solid")) {
      this.spr = img[name];
    } else {
      this.clr = name;
    }
    this.type = this.types.includes(type) ? type : "fullscreen";
    this.xspd = 0;
    this.yspd = 0;
    this.xoff = 0;
    this.yoff = 0;
    this.xscale = 1;
    this.yscale = 1;
  }
  draw() {
    if (this.type == "tiled") {
      this.xoff = wrap(this.xoff + this.xspd, 0, this.spr.width * this.xscale);
      this.yoff = wrap(this.yoff + this.yspd, 0, this.spr.height * this.yscale);
      this.defX = Math.floor(view.x / this.spr.width * this.xscale) - 1;
      this.defY = Math.floor(view.y / this.spr.height * this.yscale) - 1;
      this.x = this.defX;
      this.y = this.defY;
      while (
        this.y * this.spr.height * this.yscale <
        screen.height + view.y + this.yoff
      ) {
        this.x = this.defX;
        while (
          this.x * this.spr.width * this.xscale <
          screen.width + view.x + this.xoff
        ) {
          ctx.drawImage(
            this.spr,
            this.x * this.spr.width * this.xscale + this.xoff,
            this.y * this.spr.height * this.yscale + this.yoff,
            this.spr.width * this.xscale,
            this.spr.height * this.yscale
          );
          this.x++;
        }
        this.y++;
      }
    } else if (this.type == "fullscreen") {
      ctx.drawImage(this.spr, 0, 0, screen.width, screen.height);
    } else if (this.type == "single") {
      ctx.drawImage(this.spr, 0, 0);
    } else if (this.type == "solid") {
      ctx.fillStyle = this.clr;
      ctx.fillRect(0 + view.x, 0 + view.y, screen.width, screen.height);
    }
  }
  scroll(xspd, yspd) {
    this.xspd = xspd;
    this.yspd = yspd;
  }
  scale(xs, ys) {
    this.xscale = xs;
    this.yscale = ys;
  }
};

const Sprite = class {
  constructor(name, len, fps) {
    //img = path to image
    //twid = width of one frame //thig = height of one frame
    //fps = framerate of the sprite
    this.fps = fps;
    this.len = len;
    this.img = img[name];
    this.w = this.img.width / this.len;
    this.h = this.img.height;
    this.len--;
    this.ix = 0;
    this.attachments = [];
    this.queue = [];
    this.loop = { step: () => {} };
    if (this.fps != 0) {
      this.loop = new Loop(this.fps, () => {
        this.ix++;
        this.ix = wrap(this.ix, 0, this.len);
      });
    }
  }
  draw(x, y, w, h) {
    /** Draws the sprite with custom coords */
    this.queue = [];
    this.queue[50] = [
      () => {
        this.draw_only(x, y, w, h);
      }
    ];
    for (let i in this.attachments) {
      let temp = this.attachments[i];
      if (this.queue[temp.depth + 50] == null) {
        this.queue[temp.depth + 50] = [];
      }
      this.queue[temp.depth + 50].push(() => {
        temp.spr.draw_only(
          x + temp.offset.x,
          y + temp.offset.y,
          temp.width,
          temp.height
        );
      });
    }
    for (let e in this.queue) {
      for (let t in this.queue[e]) {
        this.queue[e][t]();
      }
    }
  }
  draw_only(x, y, w, h) {
    this.loop.step();
    ctx.drawImage(
      this.img,
      this.ix * this.w,
      0,
      this.w,
      this.h,
      x - w / 2,
      y - h / 2,
      w,
      h
    );
  }
  auto_draw(that) {
    /** Draws the sprite automatically */
    this.draw(that.x, that.y, that.width, that.height);
  }
  draw_angle(that) {
    /** Draws the sprite at an angle */
    ctx.save();
    ctx.translate(that.x, that.y);
    ctx.rotate(that.angle * Math.PI / 180);
    this.draw(0, 0, that.width, that.height);
    ctx.restore();
  }
  change(name, len) {
    /** Changes the img of the sprite */
    this.len = len;
    this.img = ing[name];
    this.w = this.img.width / this.len;
    this.h = this.img.height;
    this.ix = 0;
  }
  newfps(fps) {
    /** Changes the fps of sprite */
    this.loop = { step: () => {} };
    this.fps = fps;
    if (this.fps != 0) {
      this.loop = new Loop(fps, () => {
        this.ix += 1;
        this.ix = wrap(this.ix, 0, this.len);
      });
    }
  }
  attach(name, len, fps, depth, xr, yr, w, h) {
    this.attachments[name] = {
      spr: new Sprite(name, len, fps),
      depth: depth,
      offset: { x: xr, y: yr },
      width: w,
      height: h
    };
  }
  detach(name) {
    delete this.attachments[name];
  }
};

const TileSheet = class {
  constructor(name, w, h) {
    /** Use this class if you want to draw single
     images that are inside an tilesheet*/
    this.wid = w;
    this.hig = h;
    this.tiles = {};
    this.img = img[name];
  }
  newTile(name, x, y) {
    /** Defines a new tile */
    this.tiles[name] = { x: x, y: y };
  }
  drawTile(name, x, y, w, h) {
    /** Draws a tile by name */
    if (w === undefined) {
      w = this.wid;
    }
    if (h === undefined) {
      h = this.hig;
    }
    ctx.drawImage(
      this.img,
      this.tiles[name].x * this.wid,
      this.tiles[name].y * this.hig,
      this.wid,
      this.hig,
      x,
      y,
      w,
      h
    );
  }
};

const Thing = class {
  /** Prototype for an object */
  constructor() {
    this.name = this.constructor.name.slice(0, -1);
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0;
    this.sprite = null;
    this.type = null;
    this.angle = 0;
    this.loops = {};
    this.depth = 0;
    this.mouse = 0;
    this.persistant = false;
    this.type = null;
    this.mask = Polygon.new_rect(0, 0);
  }
  update() {
    this.step();
    if (draw_queue[this.depth + 50] == null) {
      draw_queue[this.depth + 50] = [];
    }
    draw_queue[this.depth + 50].push(() => {
      return this.draw();
    });
    if (mouse_hierarchy[this.mouse + 50] == null) {
      mouse_hierarchy[this.mouse + 50] = [];
    }
    mouse_hierarchy[this.mouse + 50].push(() => {
      return this.on_mouse();
    });
  }
  step() {}
  draw() {}
  on_mouse() {}
  on_scroll(sc) {}
  destroy() {
    /** Destroys the instance */
    obj[this.name].splice(obj[this.name].indexOf(this), 1);
  }
  show_collision_masks(color) {
    Polygon.draw(
      Polygon.rotate(
        Polygon.translate(this.mask, this.x, this.y),
        Angle.rad(this.angle)
      ),
      color == null ? null : color
    );
  }
  loop(name, tps, fn) {
    this.loops[name] = new Loop(tps, fn);
  }
  stop_loop(name) {
    delete this.loops[name];
  }
};

const Loop = class {
  /** Loop based on step count */
  constructor(tps, fn) {
    this.origin = step_count;
    this.fn = fn;
    this.tps = Math.floor(room.fps / tps);
    this.step = function() {
      if ((step_count - this.origin) % this.tps == 0) {
        this.fn();
      }
    };
  }
};

view = new View(0, 0);
